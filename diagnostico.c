#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Fecha
{
    int anio;
    int mes;
    int dia;
}fecha;

typedef struct Persona
{
    char nombre[50];
    char apellidop[50];
    char apellidom[50];
    fecha fn;
}persona;

typedef struct Trabajador
{
    persona ciudadano;
    int numEmpleado;
}trabajador;


int main(int argc, char const *argv[])
{
    trabajador trabajador;

    printf("===== Ingresa fecha de Ingreso ======\n");
    printf("Día: \n");
    scanf("%d",&trabajador.ciudadano.fn.dia);
    printf("Mes: \n");
    scanf("%d",&trabajador.ciudadano.fn.mes);
    printf("Anio: \n");
    scanf("%d",&trabajador.ciudadano.fn.anio);
    printf("===== Ingresa Nombre y Apellidos ======\n");
    printf("Nombre: \n");
    scanf("%s",trabajador.ciudadano.nombre);
    printf("Apellido Paterno: \n");
    scanf("%s",trabajador.ciudadano.apellidop);
    printf("Apellido Materno: \n");
    scanf("%s",trabajador.ciudadano.apellidom);
    trabajador.numEmpleado = 1;

    printf("===== DATOS DE PERSONALES ====\n");
    printf("NOMBRE: %s \n",trabajador.ciudadano.nombre);
    printf("APELLIDO PATERNO: %s \n",trabajador.ciudadano.apellidop);
    printf("APELLIDO MATERNO: %s \n",trabajador.ciudadano.apellidom);
    printf("DIA: %d \n",trabajador.ciudadano.fn.dia);
    printf("MES: %d \n",trabajador.ciudadano.fn.mes);
    printf("ANIO: %d \n",trabajador.ciudadano.fn.anio);
    printf("TRABAJADOR No.: %d \n",trabajador.numEmpleado);
    system("pause");
    return 0;
}
